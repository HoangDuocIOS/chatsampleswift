//
//  STEBaseCollectionViewCell.swift
//  ChatSampleSwift
//
//  Created by HoangDuoc on 1/2/19.
//  Copyright © 2019 HoangDuoc. All rights reserved.
//

import UIKit
import SnapKit
import CoreLocation

enum STECellType {
    case text
    case photo
    case video
    case audio
    case contact
    case file
    case notify
}

enum STECellMode {
    case incoming
    case outgoing
    case notify
}

let STEMessageCellAvatarImageLead: CGFloat = 3.0
let STEMessageCellAvatarImageWidth: CGFloat = 40.0

protocol STEMessageCellDelegate: AnyObject {
    func didTapContactDetailButton(cell: STEBaseCollectionViewCell, contact: STEVCard)
    func didTapFile(cell: STEBaseCollectionViewCell, fileUrl: URL, name: String)
    func didTapImage(cell: STEBaseCollectionViewCell, image: UIImage)
    func didTapPlayVideo(cell: STEBaseCollectionViewCell, url: URL)
    func didTapLocation(cell: STEBaseCollectionViewCell, location: CLLocationCoordinate2D)
}


class STEBaseCollectionViewCell: UICollectionViewCell, STEMessagePresenting {
    
    static let messageProcessingQueue = DispatchQueue(label: "com.stringee.messageprocessingqueue", attributes: .concurrent)
    static let sharedImageCache = NSCache<AnyObject, AnyObject>()
    static let sharedSizeCache = NSCache<AnyObject, AnyObject>()
    
    var message: StringeeMessage?
    
    var shoudDisplayAvatar = true
    var shouldDisplaySender = true
    var shouldDisplayMessageStatus = true
    
    let bubbleView: STEMessageBubbleView = {
        let view = STEMessageBubbleView()
        return view
    }()
    
    let avatarView: STEAvatarView = {
        let avatarView = STEAvatarView()
        return avatarView
    }()
    
    let statusView: STEMessageStatusView = {
        let view = STEMessageStatusView()
        view.layer.cornerRadius = STEMessageStatusViewCornerRadius
        view.clipsToBounds = true
        return view
    }()
    
    var bubbleWithAvatarLeadConstraint: Constraint? = nil
    var bubbleWithoutAvatarLeadConstraint: Constraint? = nil
    
    var avatarImageWidthContraint: Constraint? = nil
    var bubleViewWidthContraint: Constraint? = nil
    
    weak var delegate: STEMessageCellDelegate?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.message?.progress?.delegate = nil
        self.message = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        baseInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        baseInit()
    }
    
    private func baseInit() {
        contentView.addSubview(bubbleView)
        contentView.addSubview(avatarView)
        
        // Layout
        let maxBubbleWidth = STEMaxCellWidth + 2 * STEMessageBubbleViewHorizontalPadding
        bubbleView.snp.makeConstraints { (make) in
            bubleViewWidthContraint = make.width.equalTo(maxBubbleWidth).constraint
            make.top.equalTo(contentView.snp.top).offset(STEMessageBubbleViewVerticalMargin)
            make.bottom.equalTo(contentView.snp.bottom).offset(-STEMessageBubbleViewVerticalMargin)
            //            make.width.greaterThanOrEqualTo(<#T##other: ConstraintRelatableTarget##ConstraintRelatableTarget#>)
        }
        
        avatarView.snp.makeConstraints { (make) in
            make.bottom.equalTo(bubbleView)
            make.width.equalTo(avatarView.snp.height)
            avatarImageWidthContraint = make.width.equalTo(STEMessageCellAvatarImageWidth).constraint
        }
    }
    
    func configureCellForMode(mode: STECellMode) {
        
        switch mode {
        case .incoming:
            avatarView.snp.makeConstraints { (make) in
                make.left.equalTo(contentView.snp.left).offset(STEMessageCellAvatarImageLead)
            }
            bubbleView.snp.makeConstraints { (make) in
                bubbleWithAvatarLeadConstraint = make.left.equalTo(avatarView.snp.right).offset(STEMessageBubbleViewHorizontalMargin).constraint
                bubbleWithoutAvatarLeadConstraint = make.left.equalTo(contentView.snp.left).offset(STEMessageBubbleViewHorizontalMargin).priority(.high).constraint
            }
            bubbleWithoutAvatarLeadConstraint?.deactivate()
        case .outgoing:
            avatarView.snp.makeConstraints { (make) in
                make.right.equalTo(contentView.snp.right).offset(-STEMessageCellAvatarImageLead)
                bubbleWithAvatarLeadConstraint = make.left.equalTo(bubbleView.snp.right).offset(STEMessageBubbleViewHorizontalMargin).constraint
            }
            
            bubbleView.snp.makeConstraints { (make) in
                bubbleWithoutAvatarLeadConstraint = make.right.equalTo(contentView.snp.right).offset(-STEMessageBubbleViewHorizontalMargin).priority(.high).constraint
            }
            bubbleWithoutAvatarLeadConstraint?.deactivate()
        case .notify:
            bubbleView.snp.makeConstraints { (make) in
                make.centerX.equalTo(contentView.snp.centerX)
            }
            avatarView.isHidden = true
        }
        
        displayAvatar(shoudDisplayAvatar)
    }
    
    func updateBubbleWidth(width: CGFloat) {
        //        print("===== updateBubbleWidth \(width) -- " + self.message.content)
        bubleViewWidthContraint?.update(offset: width)
    }
    
    // MARK: - Confirm Message Presenting
    
    func present(message: StringeeMessage, shouldDisplayAvatar: Bool, shouldDisplaySender: Bool, shouldDisplayMsgStatus: Bool) {
        let name = message.sender.count > 0 ? message.sender : STEMessageNoNameString
        let avatarData = STEAvatarData(avatarImageUrl: nil, avatarInitials: name)
        self.avatarView.present(avatarItem: avatarData)
    }
    
    func shouldDisplayAvatar(should: Bool) {
        fatalError("func is not implemented in the base cell")
    }
    
    func shouldDisplaySender(should: Bool) {
        fatalError("func is not implemented in the base cell")
        
    }
    
    func shouldDisplayMsgStatus(should: Bool) {
        fatalError("func is not implemented in the base cell")
    }
    
}

// MARK: - Utils

extension STEBaseCollectionViewCell {
    func displayAvatar(_ should: Bool) {
        if should {
            bubbleWithAvatarLeadConstraint?.activate()
            bubbleWithoutAvatarLeadConstraint?.deactivate()
            avatarImageWidthContraint?.update(offset: STEMessageCellAvatarImageWidth)
        } else {
            bubbleWithAvatarLeadConstraint?.deactivate()
            bubbleWithoutAvatarLeadConstraint?.activate()
            avatarImageWidthContraint?.update(offset: 0)
        }
        
        shoudDisplayAvatar = should
    }
}

// MARK: - Cell Size Calculations

extension STEBaseCollectionViewCell {
    static func cellSizeFor(message: StringeeMessage, shouldDisplayAvatar: Bool, shouldDisplaySender: Bool, shouldDisplayMsgStatus: Bool) -> CGSize {
        if let localId = message.localIdentifier, let serverId = message.identifier, let size = sharedSizeCache.object(forKey: (localId + serverId) as AnyObject) as? CGSize {
            return size
        }
        switch message.type {
        case .createGroup:
            return cellSizeFor(notifyMsg: message)
        case .renameGroup:
            return cellSizeFor(notifyMsg: message)
        case .notify:
            return cellSizeFor(notifyMsg: message)
        case .photo:
            return cellSizeFor(photoMsg: message)
        case .location:
            return CGSize(width: STEMessageBubbleMapWidth, height: STEMessageBubbleMapHeight)
        case .contact:
            return CGSize(width: STEMessageBubbleContactWidth, height: STEMessageBubbleContactHeight)
        case .video:
            return cellSizeFor(videoMsg: message)
        case .file:
            return CGSize(width: STEMessageBubbleFileWidth, height: STEMessageBubbleFileHeight)
        case .audio:
            return CGSize(width: STEMessageBubbleAudioWidth, height: STEMessageBubbleAudioHeight)
            
        default:
            return cellSizeFor(textMsg: message, shouldDisplayAvatar, shouldDisplaySender, shouldDisplayMsgStatus)
        }
    }
    
    private static func cellSizeFor(textMsg: StringeeMessage, _ shouldDisplayAvatar: Bool, _ shouldDisplaySender: Bool, _ shouldDisplayMsgStatus: Bool) -> CGSize {
        // Tính size cho text
        let text = textMsg.content ?? STEMessageNoContentString
        let textAttributes = [NSAttributedString.Key.font: STETextCellLabelTextFont, NSAttributedString.Key.foregroundColor: STETextCellLabelTextColor]
        let textAttributeString = NSAttributedString(string: text, attributes: textAttributes)
        let textSize = STESizeFor(attributeString: textAttributeString)
        
        // Tính size cho lbName
        let name = textMsg.sender ?? STEMessageNoContentString
        let nameAttributes = [NSAttributedString.Key.font: STETextCellLabelNameFont]
        let nameAttributeString = NSAttributedString(string: name, attributes: nameAttributes)
        let nameSize = STESizeFor(attributeString: nameAttributeString, height: STEMessageBubbleViewLabelNameHeight)
        
        // Tính size cho lbTime
        let date = Date(timeIntervalSince1970: Double(textMsg.created / 1000))
        let time = STEUtils.shortTimeFormatter.string(from: date)
        
        let timeAttributes = [NSAttributedString.Key.font: STEMessageStatusViewLabelTimeFont]
        let timeAttributeString = NSAttributedString(string: time, attributes: timeAttributes)
        let timeSize = STESizeFor(attributeString: timeAttributeString, height: STEMessageBubbleViewStatusViewHeight)

        // Tính width cho statusView
        let statusViewWidth: CGFloat!
        if shouldDisplayMsgStatus {
            statusViewWidth = STEMessageStatusViewHorizontalPadding + timeSize.width + STEMessageStatusViewHorizontalPadding * 0.5 + STEMessageBubbleViewStatusViewHeight - 2 * STEMessageStatusViewVerticalPadding + STEMessageStatusViewHorizontalPadding
        } else {
            statusViewWidth = STEMessageStatusViewHorizontalPadding + timeSize.width + STEMessageStatusViewHorizontalPadding
        }
        
        // Tính width cho BubbleView
        var finalSize: CGSize = .zero
        let maxWidth = Swift.max(textSize.width, nameSize.width, statusViewWidth)
        
        var finalWidth = (maxWidth + STEMessageBubbleViewHorizontalPadding * 2).rounded(.up)
        if (finalWidth + timeSize.width) < STEMaxCellWidth {
            finalWidth += timeSize.width
        }
        
        finalSize.width = finalWidth
        
        // Tính height cho BubbleView
        if shouldDisplaySender {
            finalSize.height = (textSize.height + STEMessageBubbleViewStatusViewHeight + STEMessageBubbleViewLabelNameHeight + STEMessageBubbleViewVerticalPadding + STEMessageBubbleViewLabelNameBottomMargin + STEMessageBubbleViewVerticalMargin * 2).rounded(.up)
        } else {
            finalSize.height = (textSize.height + STEMessageBubbleViewStatusViewHeight + STEMessageBubbleViewVerticalPadding + STEMessageBubbleViewVerticalMargin * 2).rounded(.up)
        }
        
        // Cache lai size
        if let localId = textMsg.localIdentifier, let serverId = textMsg.identifier {
            sharedSizeCache.setObject(finalSize as AnyObject, forKey: (localId + serverId) as AnyObject)
        }
        
        return finalSize
    }
    
    private static func cellSizeFor(notifyMsg: StringeeMessage) -> CGSize {
        // Tính size cho text
        let text = notifyMsg.content ?? STEMessageNoContentString
        let textAttributes = [NSAttributedString.Key.font: STENotifyCellLabelNotifyFont, NSAttributedString.Key.foregroundColor: STENotifyCellLabelNotifyColor]
        let textAttributeString = NSAttributedString(string: text, attributes: textAttributes)
        var textSize = STESizeFor(attributeString: textAttributeString, width: STENotifyCellMaxTextWidth)
        
        textSize.width += STEMessageBubbleViewHorizontalPadding
        textSize.height += STEMessageBubbleViewVerticalPadding + STEMessageBubbleViewVerticalMargin * 2 + 1
        
        // Cache lai size
        if let localId = notifyMsg.localIdentifier, let serverId = notifyMsg.identifier {
            sharedSizeCache.setObject(textSize as AnyObject, forKey: (localId + serverId) as AnyObject)
        }
        
        return textSize
    }
    
    private static func cellSizeFor(photoMsg: StringeeMessage) -> CGSize {
        if let msg = photoMsg as? StringeePhotoMessage {
            let size = STEContraintImageSizeToCellSize(ratio: CGFloat(msg.ratio))
            if __CGSizeEqualToSize(size, .zero) {
                return CGSize(width: STEMessageBubbleMapWidth, height: STEMessageBubbleMapHeight)
            }
            
            // Cache lai size
            if let localId = photoMsg.localIdentifier, let serverId = photoMsg.identifier {
                sharedSizeCache.setObject(size as AnyObject, forKey: (localId + serverId) as AnyObject)
            }
            
            return size
        }
        
        return CGSize(width: STEMessageBubbleMapWidth, height: STEMessageBubbleMapHeight)
    }
    
    private static func cellSizeFor(videoMsg: StringeeMessage) -> CGSize {
        if let msg = videoMsg as? StringeeVideoMessage {
            let size = STEContraintImageSizeToCellSize(ratio: CGFloat(msg.ratio))
            if __CGSizeEqualToSize(size, .zero) {
                return CGSize(width: STEMessageBubbleMapWidth, height: STEMessageBubbleMapHeight)
            }
            
            // Cache lai size
            if let localId = msg.localIdentifier, let serverId = msg.identifier {
                sharedSizeCache.setObject(size as AnyObject, forKey: (localId + serverId) as AnyObject)
            }
            
            return size
        }
        
        return CGSize(width: STEMessageBubbleMapWidth, height: STEMessageBubbleMapHeight)
    }
}
