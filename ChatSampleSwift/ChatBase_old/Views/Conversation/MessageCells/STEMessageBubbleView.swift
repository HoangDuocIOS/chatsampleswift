//
//  STEMessageBubbleView.swift
//  ChatSampleSwift
//
//  Created by HoangDuoc on 1/2/19.
//  Copyright © 2019 HoangDuoc. All rights reserved.
//

import UIKit

let STEMessageBubbleViewCornerRadius: CGFloat = 17.0

let STEMessageBubbleViewLabelNameHeight: CGFloat = 18.0
let STEMessageBubbleViewLabelNameBottomMargin: CGFloat = 6.0

let STEMessageBubbleViewStatusViewHeight: CGFloat = 20.0

let STEMessageBubbleViewVerticalMargin: CGFloat = 2.0
let STEMessageBubbleViewHorizontalMargin: CGFloat = 6.0

let STEMessageBubbleViewHorizontalPadding: CGFloat = 8.0
let STEMessageBubbleViewVerticalPadding: CGFloat = 6.0

let STEMessageBubbleMapWidth: CGFloat = 200.0
let STEMessageBubbleMapHeight: CGFloat = 200.0

let STEMessageBubbleContactWidth: CGFloat = 200.0
let STEMessageBubbleContactHeight: CGFloat = 125.0

let STEMessageBubbleFileWidth: CGFloat = 200.0
let STEMessageBubbleFileHeight: CGFloat = 80.0

let STEMessageBubbleAudioWidth: CGFloat = 200.0
let STEMessageBubbleAudioHeight: CGFloat = 90.0

class STEMessageBubbleView: UIView {
    
    let progressView: STEProgressView = {
        let proView = STEProgressView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        proView.isHidden = true // mặc định là ẩn
        return proView
    }()
    
    init() {
        super.init(frame: .zero)
        addSubview(progressView)
        
        progressView.snp.makeConstraints { (make) in
            make.center.equalTo(self.snp.center)
            make.width.equalTo(45)
            make.height.equalTo(45)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.bringSubviewToFront(progressView)
    }
}
