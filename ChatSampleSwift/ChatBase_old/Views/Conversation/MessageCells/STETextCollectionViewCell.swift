//
//  STETextMessageCollectionViewCell.swift
//  ChatSampleSwift
//
//  Created by HoangDuoc on 1/2/19.
//  Copyright © 2019 HoangDuoc. All rights reserved.
//

import UIKit
import SnapKit

//let STETextMessageContentInset: CGFloat = 8.0

let STETextCellLabelNameFont = UIFont.boldSystemFont(ofSize: 15)
let STETextCellLabelTextFont = UIFont.systemFont(ofSize: 15)
let STETextCellLabelTextColor = UIColor.black

class STETextCollectionViewCell: STEBaseCollectionViewCell {
    
    // MARK: - Init

    let lbName: UILabel = {
        let label = UILabel()
        label.text = "User Name"
        label.font = STETextCellLabelNameFont
        label.textColor = .red
        return label
    }()
    
    let lbText: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = STETextCellLabelTextFont
        label.textColor = STETextCellLabelTextColor
        label.setContentCompressionResistancePriority(.defaultHigh + 1, for: .horizontal)
        // Để chế độ này thì việc tính size của String với core text mới chính xác
        label.lineBreakMode = .byClipping
        return label
    }()
    
    var displaySenderContraint: Constraint? = nil
    var noDisplaySenderContraint: Constraint? = nil
    
    var senderHeightContraint: Constraint? = nil
    var senderBottomMarginContraint: Constraint? = nil

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        contentView.backgroundColor = .clear
        bubbleView.layer.cornerRadius = STEMessageBubbleViewCornerRadius
        
        bubbleView.addSubview(lbName)
        bubbleView.addSubview(lbText)
        bubbleView.addSubview(statusView)
        statusView.backgroundColor = .clear
        
        // Layout
        lbName.snp.makeConstraints { (make) in
            make.top.equalTo(STEMessageBubbleViewVerticalPadding)
            make.left.equalTo(STEMessageBubbleViewHorizontalPadding)
            senderHeightContraint = make.height.equalTo(STEMessageBubbleViewLabelNameHeight).constraint
        }
        
        lbText.snp.makeConstraints { (make) in
            senderBottomMarginContraint = make.top.equalTo(lbName.snp.bottom).offset(STEMessageBubbleViewLabelNameBottomMargin).constraint
            make.left.equalTo(STEMessageBubbleViewHorizontalPadding)
            make.right.equalTo(-STEMessageBubbleViewHorizontalPadding)
            make.bottom.equalTo(statusView.snp.top).offset(0)
        }

        
        statusView.snp.makeConstraints { (make) in
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.height.equalTo(STEMessageBubbleViewStatusViewHeight)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //        print("===== Name \(lbName.frame)")
        //        print("===== Status \(statusView.frame)")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lbText.attributedText = nil
    }
    
    // MARK: - Overide Message Presenting
    
    override func present(message: StringeeMessage, shouldDisplayAvatar: Bool, shouldDisplaySender: Bool, shouldDisplayMsgStatus: Bool) {
        self.message = message
        super.present(message: message, shouldDisplayAvatar: shouldDisplayAvatar, shouldDisplaySender: shouldDisplaySender, shouldDisplayMsgStatus: shouldDisplayMsgStatus)
        
        updateBubbleWidth(width: STEBaseCollectionViewCell.cellSizeFor(message: message, shouldDisplayAvatar: shouldDisplayAvatar, shouldDisplaySender: shouldDisplaySender, shouldDisplayMsgStatus: shouldDisplayMsgStatus).width)
        self.shouldDisplayAvatar(should: shouldDisplayAvatar)
        self.shouldDisplayMsgStatus(should: shouldDisplayMsgStatus)
        self.shouldDisplaySender(should: shouldDisplaySender)
        
//        let attributes = [NSAttributedString.Key.font: STETextCellLabelTextFont, NSAttributedString.Key.foregroundColor: STETextCellLabelTextColor]
        let content = message.content ?? STEMessageNoContentString
//        let attributeString = NSAttributedString(string: content, attributes: attributes)
        
        self.lbText.text = content
        self.lbName.text = message.sender ?? STEMessageNoContentString
        statusView.update(status: message.status, timeStamp: message.created)
    }
    
    override func shouldDisplayAvatar(should: Bool) {
        displayAvatar(should)
    }
    
    override func shouldDisplaySender(should: Bool) {
        if (should) {
            senderHeightContraint?.update(offset: STEMessageBubbleViewLabelNameHeight)
            senderBottomMarginContraint?.update(offset: STEMessageBubbleViewLabelNameBottomMargin)
//            displaySenderContraint?.activate()
//            noDisplaySenderContraint?.deactivate()
        } else {
            senderHeightContraint?.update(offset: 0)
            senderBottomMarginContraint?.update(offset: 0)
//            displaySenderContraint?.deactivate()
//            noDisplaySenderContraint?.activate()
        }
        self.lbName.isHidden = !should
    }
    
    override func shouldDisplayMsgStatus(should: Bool) {
        statusView.displayMsgStatus(should)
    }
}

class STETextIncomingMessageCollectionViewCell: STETextCollectionViewCell {
    
    static let identifier = "STETextIncomingMessageCollectionViewCell"

    override init(frame: CGRect) {
        super.init(frame: frame)
        incomingCommonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        incomingCommonInit()
    }
    
    private func incomingCommonInit() {
        configureCellForMode(mode: .incoming)
        bubbleView.backgroundColor = STEColor.incomingBackground
        statusView.lbTime.textColor = STEColor.gray
    }
}

class STETextOutgoingMessageCollectionViewCell: STETextCollectionViewCell {
    
    static let identifier = "STETextOutgoingMessageCollectionViewCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        outgoingCommonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        outgoingCommonInit()
    }
    
    private func outgoingCommonInit() {
        configureCellForMode(mode: .outgoing)
        bubbleView.backgroundColor = STEColor.outgoingBackground
        statusView.lbTime.textColor = STEColor.outgoingTheme
//        statusView.ivStatus.tintColor = STEColor.green
    }
}
